package com.datajpa.entity;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.Data;

@Entity
@Table(name = "order_item")
@Data
public class OrderItem {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "orderItemId")
	private long orderItemId;
	
	@Column(name = "productPrice")
	private double productPrice;
	
	@Column(name = "productQuantity")
	private int productQuantity;
	
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "orderId")   //foreign key column name
	private OrderDetail orderdetail;
	
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "productId")
	private Product product;
	
}


