package com.datajpa.entity;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;
import lombok.Data;

@Entity
@Table(name = "payment")
@Data
public class Payment {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "paymentId")
	private int paymentId;
	
	@Column(name = "totalAmount")
	private double totalAmount;
	
	@Column(name = "paymentMode")
	private String paymentMode;

	@Column(name = "paymentDate")
	private long paymentDate;

	@Column(name = "paymentStatus")
	private boolean paymentStatus;

	@OneToOne
	@JoinColumn(name = "orderId", referencedColumnName = "orderId")
	private OrderDetail orderDetail;

	@ManyToOne(cascade = CascadeType.MERGE)
	@JoinColumn(name = "userId")
	private User user;

}


