package com.datajpa.entity;

import java.util.List;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;
import lombok.Data;

@Entity
@Table(name = "orderdetails")
@Data
public class OrderDetail {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "orderId")
	private long orderId;
	
	@Column(name = "orderNumber")
	private long orderNumber;
	
	@Column(name = "orderStatus")
	private String orderStatus;
	
	@Column(name = "orderDate")
	private long orderDate;
	
	@Column(name = "totalAmount")
	private double totalAmount;
	
	@Column(name = "numberOfOrderedProduct")
	private int numberOfOrderedProduct;
	
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "userId")
	private User user;
	
	@OneToMany(mappedBy = "orderdetail")
	private List<OrderItem> orderItemList;
	
	@OneToOne(mappedBy = "orderDetail")
	private Payment payment;

}

