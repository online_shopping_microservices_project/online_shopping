package com.datajpa.entity;

import java.util.List;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import lombok.Data;

@Entity
@Table(name = "users")
@Data
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "userId")
    private Long userId;

    @Column(name = "username")
    private String username;

    @Column(name = "userMobileNumber")
    private Long userMobileNumber;

    @Column(name = "gender")
    private String gender;

    @Column(name = "userEmail")
    private String userEmail;

    @Column(name = "password")
    private String password;

    @Column(name = "registeredAt")
    private long registeredAt;

    @Column(name = "lastLogin")
    private long lastLogin;

    @Column(name = "isEmailVerified")
    private boolean isEmailVerified;

    @Column(name = "role")
    private String role;

    @OneToMany(mappedBy = "user",fetch = FetchType.LAZY)
    private List<Address> addressList;

    @OneToMany(mappedBy = "user",fetch = FetchType.LAZY)
    private List<OrderDetail> orderDetailList;

    @OneToMany(mappedBy = "user",fetch = FetchType.LAZY)
    private List<Feedback> feedbackList;

    @OneToMany(mappedBy = "user",fetch = FetchType.LAZY)
    private List<Payment> paymentList;

    @OneToMany(mappedBy = "user",fetch = FetchType.LAZY)
    private List<Cart> cartList;

   
}

