package com.datajpa.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.Data;

@Entity
@Table(name="userproductmeta")
@Data
public class UserProductMeta {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="productMetaId")
    private Long productMetaId;
  
    @Column(name="userId")
    private Long userId;
   
    @Column(name="productId")
    private Long productId;
   
    @Column(name="productPrice")
    private double productPrice;
   
    @Column(name="productQuantity")
    private int productQuantity;

    @ManyToOne
    @JoinColumn(name = "user2")
    private User user;

    @ManyToOne
    @JoinColumn(name = "product2")
    private Product product;
}
