package com.datajpa.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.Data;

@Entity
@Table(name="address")
@Data
public class Address {
    @Id
    @Column(name="addressId")
    private Long addressId;

    @Column(name="line1")
    private String addressLine1;

    @Column(name="line2")
    private String addressLine2;

    @Column(name="city")
    private String city;

    @Column(name="state")
    private String state;

    @Column(name="pin")
    private Long pin;

    @Column(name="country")
    private String country;

    @ManyToOne
    @JoinColumn(name="userId")
    private User user;

 
}
