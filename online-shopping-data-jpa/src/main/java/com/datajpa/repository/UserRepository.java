package com.datajpa.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.datajpa.entity.User;

public interface UserRepository extends JpaRepository<User, Long>{
	User findByUserId(long userId);
	
	
	User findByUsername(String name);
	User findByUserEmail(String email);
}


