package com.user.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.user.configuration.Configuration;

@RestController
public class ConfigController {

	@Autowired
	private Configuration configuration;
	
	@GetMapping("/getIt")
	public String getValue(){
		return configuration.getPropOne() +" and "+ configuration.getPropTwo() ;
		
	}
}
