package com.user.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.user.entity.User;
import com.user.entity.UserRepository;

@RestController()
public class UserController {

	@Autowired 
	private UserRepository userRepository;
	
	
	
	@GetMapping("/getAllUser")
	public List<User> getAllUser(){
		return userRepository.findAll();
	}
	
}
