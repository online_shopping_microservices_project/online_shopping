package com.user.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.Data;

@Entity
@Table(name="address")
@Data
public class Address {
    @Id
    @Column(name="addressId")
    private Long addressId;

    @Column(name="line1")
    private String addressLine1;

    @Column(name="line2")
    private String addressLine2;

    @Column(name="city")
    private String city;

    @Column(name="state")
    private String state;

    @Column(name="pin")
    private Long pin;

    @Column(name="country")
    private String country;

    @ManyToOne
    @JoinColumn(name="userId")
    private User user;

    public Address() {
    }

    public Address(Long addressId, String addressLine1, String addressLine2, String city, String state, Long pin, String country, User user) {
        this.addressId = addressId;
        this.addressLine1 = addressLine1;
        this.addressLine2 = addressLine2;
        this.city = city;
        this.state = state;
        this.pin = pin;
        this.country = country;
        this.user = user;
    }
}

