package com.user.entity;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.Data;

@Data
@Entity
@Table(name = "cartItem")
public class CartItem {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "cartItemId")
	private int cartItemId;
	
	@Column(name = "productPrice")
	private double productPrice;
	
	@Column(name = "productQuantity")
	private int productQuantity;

	@ManyToOne(cascade = CascadeType.MERGE)
	@JoinColumn(name = "productId")
	private Product product;

	@ManyToOne(cascade = CascadeType.MERGE)
	@JoinColumn(name = "cartId")
	private Cart cart;

	public CartItem(int cartItemId, double productPrice, int productQuantity) {
		super();
		this.cartItemId = cartItemId;
		this.productPrice = productPrice;
		this.productQuantity = productQuantity;
	}

}

