package com.user.entity;

import java.util.List;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import lombok.Data;

//@Data
@Entity
@Table(name = "user")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "userId")
    private Long userId;

    @Column(name = "username")
    private String username;

    @Column(name = "userMobileNumber")
    private Long userMobileNumber;

    @Column(name = "gender")
    private String gender;

    @Column(name = "userEmail")
    private String userEmail;

    @Column(name = "password")
    private String password;

    @Column(name = "registeredAt")
    private long registeredAt;

    @Column(name = "lastLogin")
    private long lastLogin;

    @Column(name = "isEmailVerified")
    private String isEmailVerified;

    @Column(name = "role")
    private String role;

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public Long getUserMobileNumber() {
		return userMobileNumber;
	}

	public void setUserMobileNumber(Long userMobileNumber) {
		this.userMobileNumber = userMobileNumber;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getUserEmail() {
		return userEmail;
	}

	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public long getRegisteredAt() {
		return registeredAt;
	}

	public void setRegisteredAt(long registeredAt) {
		this.registeredAt = registeredAt;
	}

	public long getLastLogin() {
		return lastLogin;
	}

	public void setLastLogin(long lastLogin) {
		this.lastLogin = lastLogin;
	}

	public String getIsEmailVerified() {
		return isEmailVerified;
	}

	public void setIsEmailVerified(String isEmailVerified) {
		this.isEmailVerified = isEmailVerified;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public User(Long userId, String username, Long userMobileNumber, String gender, String userEmail, String password,
			long registeredAt, long lastLogin, String isEmailVerified, String role) {
		super();
		this.userId = userId;
		this.username = username;
		this.userMobileNumber = userMobileNumber;
		this.gender = gender;
		this.userEmail = userEmail;
		this.password = password;
		this.registeredAt = registeredAt;
		this.lastLogin = lastLogin;
		this.isEmailVerified = isEmailVerified;
		this.role = role;
	}

	public User() {

	}

    
    
//    @OneToMany(mappedBy = "user",fetch = FetchType.LAZY)
//    private List<Address> addressList;
//
//    @OneToMany(mappedBy = "user",fetch = FetchType.LAZY)
//    private List<OrderDetail> orderDetailList;
//
//    @OneToMany(mappedBy = "user",fetch = FetchType.LAZY)
//    private List<Feedback> feedbackList;
//
//    @OneToMany(mappedBy = "user",fetch = FetchType.LAZY)
//    private List<Payment> paymentList;
//
//    @OneToMany(mappedBy = "user",fetch = FetchType.LAZY)
//    private List<Cart> cartList;
  
}

