package com.user.entity;

import java.util.List;


import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import lombok.Data;

@Data
@Entity
@Table(name="product")
public class Product {

	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="productId")
	@Id
	private int id;
	
	@Column(name="productName")
	private String productName;
	
	@Column(name="productDescription")
	private String productDescription;
	
	@Column(name="productPrice")
	private double productPrice;
	
	@Column(name="totalSoldQuantity")
	private int totalSoldQuantity;
	
	@Column(name="productAddedDate")
	private long productAddedDate;
	
	@Column(name="smallQuantity")
	private int smallQuantity;
	
	@Column(name="mediumQuantity")
	private int mediumQuantity;
	
	@Column(name="largeQuantity")
	private int largeQuantity;
	
	@Column(name="extraLargeQuantity")
	private int extraLargeQuantity;
	
	@Column(name="size")
	private String size;

	@Column(name = "imageUrl")
	private String imageUrl;

	@Column(name = "productDetails")
	private String productDetails;
	

	@ManyToOne(cascade = CascadeType.ALL) // we can write this field in toString
	@JoinColumn(name = "categoryId")
	private Category category;
	
	
	@OneToMany(mappedBy = "product")	//don't write this field in toString else it will give stackOverflow exception 
	private List<OrderItem> orderItemList;
	
//	@OneToMany(mappedBy = "product")//don't write this field in toString else it will give stackOverflow exception
//	private List<UserProductMeta> userProductMetaList;

	@OneToMany(fetch = FetchType.EAGER, mappedBy = "product")//don't write this field in toString else it will give stackOverflow exception
	private List<Feedback> feedbackList;

	@OneToMany(mappedBy = "product")//don't write this field in toString else it will give stackOverflow exception
	private List<CartItem> cartItemList;
	
}

