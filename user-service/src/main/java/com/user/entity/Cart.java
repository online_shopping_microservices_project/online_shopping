package com.user.entity;

import java.util.List;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import lombok.Data;

@Data
@Entity
@Table(name = "cart")
public class Cart {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "cartId")
	private int cartId;
	@Column(name = "quantity")
	private int quantity;
	@Column(name = "isPurchased")
	private boolean isPurchased;

	@ManyToOne(cascade = CascadeType.MERGE)
	@JoinColumn(name = "userId")
	private User user;

	@OneToMany(mappedBy = "cart")
	private List<CartItem> cartItemList;

	public Cart(int cartId, int quantity, boolean isPurchased) {
		super();
		this.cartId = cartId;
		this.quantity = quantity;
		this.isPurchased = isPurchased;
	}

	public Cart() {
		super();
	}

}


