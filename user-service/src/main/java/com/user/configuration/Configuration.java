package com.user.configuration;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import lombok.Data;
@Component
@Data
@ConfigurationProperties("user-service")
public class Configuration {
	private int propOne ;
	private int propTwo ;

	
	
}
