package com.example.demo.entity;

import java.util.Locale.Category;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table(name="product")
public class Product {

	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="productId")
	@Id
	private int id;
	
	@Column(name="productName")
	private String productName;
	
	@Column(name="productDescription")
	private String productDescription;
	
	@Column(name="productPrice")
	private double productPrice;
	
	@Column(name="totalSoldQuantity")
	private int totalSoldQuantity;
	
	@Column(name="productAddedDate")
	private long productAddedDate;
	
	@Column(name="smallQuantity")
	private int smallQuantity;
	
	@Column(name="mediumQuantity")
	private int mediumQuantity;
	
	@Column(name="largeQuantity")
	private int largeQuantity;
	
	@Column(name="extraLargeQuantity")
	private int extraLargeQuantity;
	
	@Column(name="size")
	private String size;

	@Column(name = "imageUrl")
	private String imageUrl;

	@Column(name = "productDetails")
	private String productDetails;
	

//	@ManyToOne(cascade = CascadeType.ALL) // we can write this field in toString
//	@JoinColumn(name = "categoryId")
//	private Category category;
	
	
//	@OneToMany(mappedBy = "product")	//don't write this field in toString else it will give stackOverflow exception 
//	private List<OrderItem> orderItemList;
//	
//	@OneToMany(mappedBy = "product")//don't write this field in toString else it will give stackOverflow exception
//	private List<UserProductMeta> userProductMetaList;
//
//	@OneToMany(fetch = FetchType.EAGER, mappedBy = "product")//don't write this field in toString else it will give stackOverflow exception
//	private List<Feedback> feedbackList;
//
//	@OneToMany(mappedBy = "product")//don't write this field in toString else it will give stackOverflow exception
//	private List<CartItem> cartItemList;
//	
	
	public Product() {
		super();
	}

	public Product(int id) {
		super();
		this.id = id;
	}





	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getProductName() {
		return productName;
	}


	public void setProductName(String productName) {
		this.productName = productName;
	}


	public String getProductDescription() {
		return productDescription;
	}


	public void setProductDescription(String productDescription) {
		this.productDescription = productDescription;
	}


	public double getProductPrice() {
		return productPrice;
	}


	public void setProductPrice(double productPrice) {
		this.productPrice = productPrice;
	}


	public int getTotalSoldQuantity() {
		return totalSoldQuantity;
	}


	public void setTotalSoldQuantity(int totalSoldQuantity) {
		this.totalSoldQuantity = totalSoldQuantity;
	}



	public int getSmallQuantity() {
		return smallQuantity;
	}


	public void setSmallQuantity(int smallQuantity) {
		this.smallQuantity = smallQuantity;
	}


	public int getMediumQuantity() {
		return mediumQuantity;
	}


	public void setMediumQuantity(int mediumQuantity) {
		this.mediumQuantity = mediumQuantity;
	}


	public int getLargeQuantity() {
		return largeQuantity;
	}


	public void setLargeQuantity(int largeQuantity) {
		this.largeQuantity = largeQuantity;
	}


	public int getExtraLargeQuantity() {
		return extraLargeQuantity;
	}


	public void setExtraLargeQuantity(int extraLargeQuantity) {
		this.extraLargeQuantity = extraLargeQuantity;
	}

	

	public String getSize() {
		return size;
	}


	public void setSize(String size) {
		this.size = size;
	}


	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public String getProductDetails() {
		return productDetails;
	}

	public void setProductDetails(String productDetails) {
		this.productDetails = productDetails;
	}


}


