package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;

import com.example.demo.test.Demo;

@SpringBootApplication
@EnableConfigServer
public class ConfigServiceApplication {

	@Autowired
	public Demo demo;
	
	public static void main(String[] args) {
		SpringApplication.run(ConfigServiceApplication.class, args);
	
	}

}
