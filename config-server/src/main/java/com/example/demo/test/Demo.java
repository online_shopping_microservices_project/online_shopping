package com.example.demo.test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

@Service
@PropertySource("classpath:application.properties")
public class Demo {

	@Autowired
	private static Environment env;

	public static String getIt() {

		return env.getProperty("user.test");

	}

}
